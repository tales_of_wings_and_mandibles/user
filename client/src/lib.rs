use rest_client::RestClient;
use user_domain::app_user;

#[derive(Clone)]
pub struct Client {
	rest_client: RestClient,
	service_base_url: String,
}

impl Client {
	pub fn new(rest_client: RestClient, service_base_url: String) -> Self {
		Self {
			rest_client,
			service_base_url,
		}
	}

	pub async fn create_user(&self, user: &app_user::ActiveModel) {
		self.rest_client
			.post_with_json_body(&self.service_base_url, "/user", user)
			.await
	}

	pub async fn get_user(&self, user_id: i32) -> app_user::Model {
		self.rest_client
			.get(&self.service_base_url, format!("/user/{user_id}"))
			.await
	}

	pub async fn delete_user(&self, user_id: i32) {
		self.rest_client
			.delete(&self.service_base_url, format!("/user/{user_id}"))
			.await
	}
}
