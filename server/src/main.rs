use std::error::Error;

use axum::{
	extract::{Path, State},
	http::StatusCode,
	response::IntoResponse,
	routing::{get, post},
	Json, Router,
};
use sqlx::{PgPool, Pool, Postgres};
use tokio::net::TcpListener;

use user_domain::app_user;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	let db = PgPool::connect("postgres://towam:towam@towam_postgres:5432").await?;

	let state = AppState { db };

	let app = Router::new()
		.route("/user", post(create_user))
		.route("/user/:id", get(get_user).delete(delete_user))
		.route("/health", get(health))
		.with_state(state);

	let listener = TcpListener::bind("0.0.0.0:3000").await.unwrap();

	axum::serve(listener, app).await.unwrap();

	Ok(())
}

async fn create_user(
	State(state): State<AppState>,
	Json(user_details): Json<app_user::ActiveModel>,
) -> impl IntoResponse {
	let created_user_id = sqlx::query!(
		"INSERT INTO app_user VALUES ( $1 ) RETURNING id",
		sqlx::types::Json(user_details) as _
	)
	.fetch_one(&state.db)
	.await
	.unwrap()
	.id;

	let created_user = sqlx::query_as!(
		app_user::Model,
		"SELECT * FROM app_user WHERE id = $1",
		created_user_id
	)
	.fetch_one(&state.db)
	.await
	.unwrap();

	(StatusCode::CREATED, Json(created_user))
}

async fn get_user(Path(id): Path<i32>, State(state): State<AppState>) -> impl IntoResponse {
	let user = sqlx::query_as!(app_user::Model, "SELECT * FROM app_user WHERE id = $1", id)
		.fetch_one(&state.db)
		.await
		.unwrap();

	Json(user)
}

async fn delete_user(Path(id): Path<i32>, State(state): State<AppState>) -> impl IntoResponse {
	sqlx::query!("DELETE FROM app_user WHERE id = $1", id)
		.execute(&state.db)
		.await
		.unwrap();

	StatusCode::OK
}

async fn health(State(state): State<AppState>) -> impl IntoResponse {
	let is_database_connected = sqlx::query!("SELECT * FROM app_user LIMIT 1")
		.fetch_one(&state.db)
		.await
		.is_ok();

	if is_database_connected {
		(StatusCode::OK, "ok")
	} else {
		(StatusCode::SERVICE_UNAVAILABLE, "nok")
	}
}

#[derive(Clone)]
struct AppState {
	db: Pool<Postgres>,
}
